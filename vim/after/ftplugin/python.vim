if executable("python3") && executable("black")
    setlocal formatprg=black\ -l\ 79\ --quiet\ -\ 2>/dev/null
endif
